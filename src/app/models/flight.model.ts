import { Ticket } from './ticket.model';

export class Flight {
  arrival: Date;
  departure: Date;
  ticket: Ticket;
  tickets: Ticket[];
  ticketsCount: number;

  constructor(init: {
    arrival: Date;
    departure: Date;
    tickets: Ticket[];
    ticketsCount: number;
  }) {
    this.arrival = init.arrival;
    this.departure = init.departure;
    this.tickets = init.tickets;
    this.ticketsCount = init.ticketsCount;
  }

  static createFromVndEntity(vndEntity: any) {
    const tickets = [];

    vndEntity.fares.forEach(fareVndEntity => {
      tickets.push(Ticket.createFromVndEntity(fareVndEntity));
    });

    return new Flight({
      arrival: vndEntity.arrival,
      departure: vndEntity.departure,
      tickets: tickets,
      ticketsCount: vndEntity.remainingTickets,
    });
  }
}
