import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { Flight } from '../../models/flight.model';
import { Ticket } from '../../models/ticket.model';

@Component({
  selector: 'app-flight-select',
  templateUrl: './flight-select.component.html',
  styleUrls: ['./flight-select.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FlightSelectComponent {
  @Input() flight: Flight;

  @Input() model: Flight;
  @Output() modelChange: EventEmitter<Flight> = new EventEmitter();

  onSelectFlight(flight: Flight, ticket: Ticket) {
    this.model = flight;
    this.model.ticket = ticket;

    this.modelChange.emit(this.model);
  }
}
