import { Injectable } from '@angular/core';
import { validate } from 'class-validator';
import { IMyDate, IMyDefaultMonth } from 'mydatepicker';
import { Travel } from '../models/travel.model';
import { ErrorConverterService } from './error-converter.service';

@Injectable()
export class TravelService {
  constructor(private errorConverterService: ErrorConverterService) {}

  validate(travel: Travel, validationGroup: string): Promise<any> {
    return validate(travel, {
      groups: [
        validationGroup
      ]
    }).then(errors => {
      return this.errorConverterService.convert(errors);
    });
  }

  getReturnAtConstraints(travel: Travel): {
    disableUntil: IMyDate;
    defaultMonth: IMyDefaultMonth;
  } {
    const departureAt = new Date(travel.departureAt);

    const returnAtDisableUntil = {
      year: departureAt.getFullYear(),
      month: departureAt.getMonth() + 1,
      day: departureAt.getDate()
    };

    const returnAtDefaultMonth = {
      defMonth: `${returnAtDisableUntil.year}-${returnAtDisableUntil.month}`
    };

    return {
      disableUntil: returnAtDisableUntil,
      defaultMonth: returnAtDefaultMonth
    };
  }
}
