import { Component, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-errors',
  templateUrl: './errors.component.html',
  styleUrls: ['./errors.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ErrorsComponent {
  @Input() errors: string[];
}
