export interface StorageServiceInterface {
  set(data: any);
  get();
}
