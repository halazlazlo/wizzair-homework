import { Injectable } from '@angular/core';
import { Travel } from '../../models/travel.model';
import { StorageServiceInterface } from './storage.service.interface';

@Injectable()
export class TravelStorageService implements StorageServiceInterface {
  private key = 'flight-search';

  get(): Travel {
    const data: any = localStorage.getItem(this.key);

    if (data) {
      return new Travel(JSON.parse(data));
    }

    return null;
  }

  set(flight: Travel) {
    localStorage.setItem(this.key, JSON.stringify(flight));
  }
}
