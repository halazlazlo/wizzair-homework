import { DatePipe } from '@angular/common';
import { Station } from '../../models/station.model';
import { Travel } from '../../models/travel.model';
import { StationRepositoryInterface } from '../../repositories/station.repository.interface';
import { ErrorConverterService } from '../../services/error-converter.service';
import { StorageServiceInterface } from '../../services/storages/storage.service.interface';
import { TravelService } from '../../services/travel.service';
import { FlightSearchPanelComponent } from './flight-search-panel.component';

describe('flight-search-panel', () => {
  let flightSearchPanel: FlightSearchPanelComponent;

  let stationRepository: StationRepositoryInterface;
  let flightStorage: StorageServiceInterface;
  let travelService: TravelService;

  beforeEach(() => {
    // mock repo
    stationRepository = {
      findAll: () => Promise.resolve([
        new Station({
          name: 'Budapest',
          iata: 'BUD',
          connections: [
            'BUC'
          ]
        }),
        new Station({
          name: 'Bucharest',
          iata: 'BUC'
        }),
      ]),
    };

    // mock storage
    flightStorage = {
      set: () => {},
      get: () => null
    };

    travelService = new TravelService(new ErrorConverterService());

    flightSearchPanel = new FlightSearchPanelComponent(stationRepository, flightStorage, travelService);
  });

  describe('init', () => {
    it('should load flight from storage on init', done => {
      spyOn(flightStorage, 'get');

      flightSearchPanel.ngOnInit();

      setTimeout(() => {
        expect(flightStorage.get).toHaveBeenCalled();

        done();
      }, 1);
    });

    it('should set departureAt default as today is not set already', done => {
      flightSearchPanel.ngOnInit();

      setTimeout(() => {
        const datePipe = new DatePipe('en-US');

        expect(flightSearchPanel.travel.departureAt).toBe(datePipe.transform(Date.now(), 'yyyy-MM-dd'));

        done();
      }, 1);
    });
  });

  describe('change', () => {
    it('should clear destination when origin is changed', () => {
      flightSearchPanel.travel = new Travel({
        origin: new Station({
          name: 'Budapest',
          iata: 'BUD',
          connections: []
        }),
        destination: new Station({
          name: 'Bucharest',
          iata: 'BUC',
          connections: []
        }),
      });

      flightSearchPanel.onOriginChange();

      expect(flightSearchPanel.travel.destination).toBeNull();
    });

    it('should not have destination options if origin dont have connections', done => {
      flightSearchPanel.travel = new Travel({
        origin: new Station({
          name: 'Bucharest',
          iata: 'BUD',
          connections: []
        })
      });

      flightSearchPanel.ngOnInit();

      setTimeout(() => {
        flightSearchPanel.onOriginChange();

        expect(flightSearchPanel.destinationOptions.length).toBe(0);

        done();
      }, 1);
    });

    it('should have destination options if origin has connections', done => {
      flightSearchPanel.travel = new Travel({
        origin: new Station({
          name: 'Budapest',
          iata: 'BUD',
          connections: ['BUC']
        })
      });

      flightSearchPanel.ngOnInit();

      setTimeout(() => {
        flightSearchPanel.onOriginChange();

        expect(flightSearchPanel.destinationOptions.length).toBe(1);
        expect(flightSearchPanel.destinationOptions[0].name).toBe('Bucharest');

        done();
      }, 1);
    });

    it('should the returnAt date be available from departure day', () => {
      flightSearchPanel.travel = new Travel({
        departureAt: '2019-01-01'
      });

      flightSearchPanel.onDepartureAtChange();

      expect(flightSearchPanel.returnAtConstraints.disableUntil).toEqual({
        year: 2019,
        month: 1,
        day: 1
      });

      expect(flightSearchPanel.returnAtConstraints.defaultMonth.defMonth).toBe('2019-1');
    });

    it('should clear returnAt if departure changes larger', () => {
      flightSearchPanel.travel = new Travel({
        departureAt: '2018-08-12',
        returnAt: '2017-08-11'
      });

      flightSearchPanel.onDepartureAtChange();

      expect(flightSearchPanel.travel.returnAt).toBeNull();
    });
  });

  describe('save', () => {
    beforeEach(() => {
      flightSearchPanel.travel = new Travel({
        origin: new Station({
          name: 'Budapest',
          iata: 'BUD',
          connections: []
        }),
        destination: new Station({
          name: 'Bucharest',
          iata: 'BUC',
          connections: []
        }),
        departureAt: '2018-08-10',
        returnAt: '2018'
      });
    });

    it('should save flight on origin change', () => {
      spyOn(flightStorage, 'set');

      flightSearchPanel.onOriginChange();

      expect(flightStorage.set).toHaveBeenCalled();
    });

    it('should save flight on destination change', () => {
      spyOn(flightStorage, 'set');

      flightSearchPanel.onDestinationChange();

      expect(flightStorage.set).toHaveBeenCalled();
    });

    it('should save flight on departureAt change', () => {
      spyOn(flightStorage, 'set');

      flightSearchPanel.onDepartureAtChange();

      expect(flightStorage.set).toHaveBeenCalled();
    });

    it('should save flight on returnAt change', () => {
      spyOn(flightStorage, 'set');

      flightSearchPanel.onReturnAtChange();

      expect(flightStorage.set).toHaveBeenCalled();
    });
  });
});
