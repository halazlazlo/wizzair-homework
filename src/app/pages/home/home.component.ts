import { Component, ViewEncapsulation } from '@angular/core';
import { Travel } from '../../models/travel.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent {
  travel: Travel;

  isFlightSearchPanelVisible = true;
  isFlightSearchResultsVisible: boolean;
  isTravelOverviewVisible: boolean;

  onSearch() {
    this.isFlightSearchPanelVisible = false;
    this.isFlightSearchResultsVisible = true;
  }

  onFlightsSelected() {
    this.isFlightSearchResultsVisible = false;
    this.isTravelOverviewVisible = true;
  }
}
