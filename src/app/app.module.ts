import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgSelectModule } from '@ng-select/ng-select';
import { MyDatePickerModule } from 'mydatepicker';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';
import { BtnComponent } from './components/btn/btn.component';
import { FlightSearchPanelComponent } from './components/flight-search-panel/flight-search-panel.component';
import { FlightSearchResultsComponent } from './components/flight-search-results/flight-search-results.component';
import { FlightSelectComponent } from './components/flight-select/flight-select.component';
import { DatePickerComponent } from './components/form-elements/date-picker/date-picker.component';
import { ErrorsComponent } from './components/form-elements/errors/errors.component';
import { FormGroupComponent } from './components/form-elements/form-group/form-group.component';
import { LabelComponent } from './components/form-elements/label/label.component';
import { SelectComponent } from './components/form-elements/select/select.component';
import { HeaderComponent } from './components/header/header.component';
import { LoaderComponent } from './components/loader/loader.component';
import { LogoComponent } from './components/logo/logo.component';
import { PanelComponent } from './components/panel/panel.component';
import { TravelOverviewComponent } from './components/travel-overview/travel-overview.component';
import { HomeComponent } from './pages/home/home.component';
import { FlightRepository } from './repositories/flight.repository';
import { StationRepository } from './repositories/station.repository';
import { ErrorConverterService } from './services/error-converter.service';
import { HttpService } from './services/http.service';
import { LoaderService } from './services/loader.service';
import { TravelStorageService } from './services/storages/flight-storage.service';
import { TravelService } from './services/travel.service';
import { HeadingComponent } from './components/heading/heading.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LogoComponent,
    HomeComponent,
    PanelComponent,
    FormGroupComponent,
    BtnComponent,
    SelectComponent,
    LabelComponent,
    LoaderComponent,
    ErrorsComponent,
    FlightSearchPanelComponent,
    DatePickerComponent,
    FlightSearchResultsComponent,
    FlightSelectComponent,
    TravelOverviewComponent,
    HeadingComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,

    NgSelectModule,
    MyDatePickerModule
  ],
  providers: [
    HttpService,
    StationRepository,
    FlightRepository,

    LoaderService,
    ErrorConverterService,
    TravelStorageService,
    TravelService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
