import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as qs from 'qs';
import { LoaderService } from './loader.service';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  private baseUrl = 'https://mock-air.herokuapp.com';

  constructor(
    private http: HttpClient,
    private loaderService: LoaderService
  ) {}

  get(url: string, params?: object, deserializerOptions?: any): Promise<any> {
    this.loaderService.add(url);

    const queryString = params ? `?${qs.stringify(params, { encode: false })}` : '';

    return <any>this.http
      .get(this.baseUrl + url + queryString)
      .toPromise()
      .then((response: HttpResponse<any>) => {
        this.loaderService.finish(url);

        return response;
      });
  }
}
