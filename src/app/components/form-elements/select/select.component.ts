import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SelectComponent {
  @Input() label: string;
  @Input() options: any;
  @Input() model: string;
  @Output() modelChange: EventEmitter<any> = new EventEmitter();

  @Input() valueField?;
  @Input() labelField ? = 'label';

  @Input() errors?: string[];

  onModelChange() {
    this.modelChange.emit(this.model);
  }
}
