import { Injectable } from '@angular/core';
import { Flight } from '../models/flight.model';
import { HttpService } from '../services/http.service';

@Injectable()
export class FlightRepository {
  constructor(private http: HttpService) {}

  findBy(params: {
    departureStation: string,
    arrivalStation: string,
    date: string
  }): Promise<Flight[]> {
    return this.http.get('/search', params).then(vndEntities => {
      const flights = [];

      vndEntities.forEach(vndEntity => {
        flights.push(Flight.createFromVndEntity(vndEntity));
      });

      return flights;
    });
  }
}
