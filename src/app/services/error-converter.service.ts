import { Injectable } from '@angular/core';
import { ValidationError } from 'class-validator';

@Injectable()
export class ErrorConverterService {
  convert(errors: ValidationError[]) {
    const convertedErrors = {};

    errors.forEach((error: ValidationError) => {
      if (error.children.length) {
        convertedErrors[error.property] = this.convert(error.children);
      } else {
        if (!convertedErrors[error.property]) {
          convertedErrors[error.property] = [];
        }

        Object.keys(error.constraints).forEach(key => {
          convertedErrors[error.property].push({
            message: error.constraints[key]
          });
        });
      }
    });

    return convertedErrors;
  }
}
