import { Component, Input, ViewEncapsulation } from '@angular/core';
import { Travel } from '../../models/travel.model';

@Component({
  selector: 'app-travel-overview',
  templateUrl: './travel-overview.component.html',
  styleUrls: ['./travel-overview.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TravelOverviewComponent {
  @Input() travel: Travel;
}
