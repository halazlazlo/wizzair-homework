import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { IMyDate, IMyDefaultMonth, IMyDpOptions } from 'mydatepicker';

@Component({
  selector: 'app-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DatePickerComponent implements OnInit {
  @Input() model: string;
  @Output() modelChange: EventEmitter<string> = new EventEmitter();

  @Input() label: string;

  private _disableUntil: IMyDate;
  @Input()
  set disableUntil(disableUntil: IMyDate) {
    if (disableUntil) {
      this.myDatePickerOptions = Object.assign({}, this.myDatePickerOptions, {
        disableUntil: disableUntil
      });
    }
  }
  get disableUntil(): IMyDate {
    return this._disableUntil;
  }

  @Input() defaultMonth: IMyDefaultMonth;

  @Input() errors?: string[];

  ngModel: any;
  myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'yyyy-mm-dd',
    openSelectorOnInputClick: true,
    editableDateField: false,
    showClearDateBtn: false,
    showTodayBtn: false,
    selectorWidth: '300px',
    selectorHeight: '280px',
    yearSelector: false
  };

  ngOnInit() {
    this.initModel();
  }

  onModelChange() {
    this.model = this.ngModel.formatted;

    this.modelChange.emit(this.model);
  }

  private initModel() {
    if (this.model) {
      const date = new Date(this.model);

      this.ngModel = {
        date: {
          year: date.getFullYear(),
          month: date.getMonth() + 1,
          day: date.getDate()
        }
      };
    }
  }
}
