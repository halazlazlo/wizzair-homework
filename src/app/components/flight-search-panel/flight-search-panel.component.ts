import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { IMyDate } from 'mydatepicker';
import { Station } from '../../models/station.model';
import { Travel } from '../../models/travel.model';
import { StationRepository } from '../../repositories/station.repository';
import { StationRepositoryInterface } from '../../repositories/station.repository.interface';
import { TravelStorageService } from '../../services/storages/flight-storage.service';
import { StorageServiceInterface } from '../../services/storages/storage.service.interface';
import { TravelService } from '../../services/travel.service';

@Component({
  selector: 'app-flight-search-panel',
  templateUrl: './flight-search-panel.component.html',
  styleUrls: ['./flight-search-panel.component.scss']
})
export class FlightSearchPanelComponent implements OnInit {
  @Input() travel: Travel;
  @Output() travelChange: EventEmitter<Travel> = new EventEmitter();

  @Output() search: EventEmitter<any> = new EventEmitter();

  stations: Station[];

  travelErrors = {};

  originOptions: Station[];
  destinationOptions: Station[];

  departureAtDisableUntil: IMyDate;
  returnAtConstraints;

  constructor(
    @Inject(StationRepository) private stationRepository: StationRepositoryInterface,
    @Inject(TravelStorageService) private travelStorage: StorageServiceInterface,
    private travelService: TravelService
  ) {}

  ngOnInit() {
    this.initStations();
    this.initFlight();
    this.initDatepickers();
  }

  onOriginChange() {
    this.filterDestionationOptions();

    this.clearDestination();

    this.saveFlight();
  }

  onDestinationChange() {
    this.saveFlight();
  }

  onDepartureAtChange() {
    this.initReturnAtConstraints();

    if (this.travel.returnAt && new Date(this.travel.returnAt) <= new Date(this.travel.departureAt)) {
      this.travel.returnAt = null;
    }

    this.saveFlight();
  }

  onReturnAtChange() {
    this.saveFlight();
  }

  onSubmit() {
    this.travelService.validate(this.travel, 'flight-search-panel').then(errors => {
      this.travelErrors = errors;

      if (!Object.keys(errors).length) {
        this.travelChange.emit(this.travel);

        this.search.emit();
      }
    });
  }

  private initFlight() {
    const flight = this.travelStorage.get();

    if (flight) {
      this.travel = flight;
    } else if (!this.travel) {
      this.travel = new Travel();
    }

    if (!this.travel.departureAt) {
      const datePipe = new DatePipe('en-US');

      this.travel.departureAt = datePipe.transform(Date.now(), 'yyyy-MM-dd');
    }
  }

  private initStations() {
    this.stationRepository.findAll().then(stations => {
      this.stations = stations;
      this.originOptions = stations;

      this.filterDestionationOptions();
    });
  }

  private initDatepickers() {
    const now = new Date();

    this.departureAtDisableUntil = {
      year: now.getFullYear(),
      month: now.getMonth() + 1,
      day: now.getDate() - 1
    };

    this.initReturnAtConstraints();
  }

  private initReturnAtConstraints() {
    this.returnAtConstraints = this.travelService.getReturnAtConstraints(this.travel);
  }

  private filterDestionationOptions() {
    if (this.travel.origin && this.stations) {
      const connections = [];

      if (this.travel.origin.connections) {
        this.travel.origin.connections.forEach(connectionIata => {
          const connection = this.stations.find(station => station.iata === connectionIata);

          connections.push(connection);
        });
      }

      this.destinationOptions = connections;
    }
  }

  private clearDestination() {
    this.travel.destination = null;
  }

  private saveFlight() {
    this.travelStorage.set(this.travel);
  }
}
