import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class LoaderService {
  private pile: string[] = [];
  private subject = new BehaviorSubject<boolean>(false);

  isLoading$: Observable<boolean> = this.subject.asObservable();

  add(key: string) {
    this.pile.push(key);

    this.subject.next(true);
  }

  finish(key) {
    this.pile.splice(this.pile.indexOf(key), 1);

    if (!this.pile.length) {
      this.subject.next(false);
    }
  }

  flush() {
    this.pile = [];

    this.subject.next(false);
  }
}
