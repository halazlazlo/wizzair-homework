import { Station } from '../models/station.model';

export interface StationRepositoryInterface {
  findAll(): Promise<Station[]>;
}
