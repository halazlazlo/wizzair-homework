import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { IMyDate, IMyDefaultMonth } from 'mydatepicker';
import { Flight } from '../../models/flight.model';
import { Travel } from '../../models/travel.model';
import { FlightRepository } from '../../repositories/flight.repository';
import { TravelService } from '../../services/travel.service';

@Component({
  selector: 'app-flight-search-results',
  templateUrl: './flight-search-results.component.html',
  styleUrls: ['./flight-search-results.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FlightSearchResultsComponent implements OnInit {
  @Input() travel: Travel;
  @Output() travelChange: EventEmitter<Travel> = new EventEmitter();

  @Output() continue: EventEmitter<any> = new EventEmitter();

  travelErrors = {};

  departureFlights: Flight[];
  arrivalFlights: Flight[];

  // if returnAtIsNot selected
  returnAt: string;
  returnAtConstraints: {
    disableUntil: IMyDate;
    defaultMonth: IMyDefaultMonth;
  };

  constructor(
    private flightRepository: FlightRepository,
    private travelService: TravelService
  ) { }

  ngOnInit() {
    // find departure flights
    this.flightRepository.findBy({
      departureStation: this.travel.origin.iata,
      arrivalStation: this.travel.destination.iata,
      date: this.travel.departureAt
    }).then(flights => {
      this.departureFlights = flights;
    });

    // find arrival flights
    if (this.travel.returnAt) {
      this.initArrivalFlights();
    } else {
      this.returnAtConstraints = this.travelService.getReturnAtConstraints(this.travel);
    }
  }

  onContinue() {
    this.travelService.validate(this.travel, 'flight-select').then(errors => {
      this.travelErrors = errors;

      if (!Object.keys(errors).length) {
        this.travelChange.emit(this.travel);
        this.continue.emit();
      }
    });
  }

  onReturnAtChange() {
    if (this.returnAt) {
      this.travel.returnAt = this.returnAt;

      this.initArrivalFlights();

      this.travelChange.emit(this.travel);
    }
  }

  initArrivalFlights() {
    this.flightRepository.findBy({
      departureStation: this.travel.destination.iata,
      arrivalStation: this.travel.origin.iata,
      date: this.travel.returnAt
    }).then(flights => {
      this.arrivalFlights = flights;
    });
  }
}
