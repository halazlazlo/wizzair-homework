import { IsNotEmpty, ValidateIf } from 'class-validator';
import { Flight } from './flight.model';
import { Station } from './station.model';

export class Travel {
  @IsNotEmpty({
    groups: [
      'flight-search-panel'
    ]
  })
  origin?: Station;

  @IsNotEmpty({
    groups: [
      'flight-search-panel'
    ]
  })
  destination?: Station;

  @IsNotEmpty({
    groups: [
      'flight-search-panel'
    ]
  })
  departureAt?: string;

  @IsNotEmpty({
    groups: [
      'flight-select-return-at'
    ]
  })
  returnAt?: string;

  @IsNotEmpty({
    groups: [
      'flight-select'
    ]
  })
  departureFlight: Flight;

  @ValidateIf(flight => !!flight.returnAt, {
    groups: [
      'flight-select'
    ]
  })
  @IsNotEmpty({
    groups: [
      'flight-select'
    ]
  })
  returnFlight: Flight;

  constructor(init?: {
    origin?: Station;
    destination?: Station;
    departureAt?: string;
    returnAt?: string;
  }) {
    this.origin = init ? init.origin : null;
    this.destination = init ? init.destination : null;
    this.departureAt = init ? init.departureAt : null;
    this.returnAt = init ? init.returnAt : null;
  }

  getPrice() {
    let price = this.departureFlight.ticket.price;
    if (this.returnFlight) {
      price += this.returnFlight.ticket.price;
    }

    return price;
  }
}
