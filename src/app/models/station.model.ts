export class Station {
  name?: string;
  iata?: string;
  connections?: string[];

  constructor(init: {
    name?: string;
    iata?: string;
    connections?: string[];
  }) {
    this.name = init.name ? init.name : null;
    this.iata = init.iata ? init.iata : null;
    this.connections = init.connections ? init.connections : null;
  }

  static createFromVndEntity(vndEntity): Station {
    const connections = [];

    if (vndEntity.connections) {
      vndEntity.connections.forEach(connectionVndEntity => {
        connections.push(connectionVndEntity.iata);
      });
    }

    return new Station({
      name: vndEntity.shortName,
      iata: vndEntity.iata,
      connections: connections
    });
  }
}
