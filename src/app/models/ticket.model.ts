export class Ticket {
  id: string;
  price: number;
  bundle: string;

  constructor(init: {
    id: string;
    price: number;
    bundle: string;
  }) {
    this.id = init.id;
    this.price = init.price;
    this.bundle = init.bundle;
  }

  static createFromVndEntity(vndEntity: any) {
    return new Ticket({
      id: vndEntity.fareSellKey,
      price: vndEntity.price,
      bundle: vndEntity.bundle
    });
  }
}
