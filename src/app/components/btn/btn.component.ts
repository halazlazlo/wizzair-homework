import { Component, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-btn',
  templateUrl: './btn.component.html',
  styleUrls: ['./btn.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BtnComponent {
  @Input() label: string;
  @Input() isFullWidth: boolean;
}
