import { Injectable } from '@angular/core';
import { Station } from '../models/station.model';
import { HttpService } from '../services/http.service';
import { StationRepositoryInterface } from './station.repository.interface';

@Injectable()
export class StationRepository implements StationRepositoryInterface {
  constructor(private http: HttpService) {}

  findAll(): Promise<Station[]> {
    return this.http.get('/asset/stations').then(vndEntities => {
      const stations = [];

      vndEntities.forEach(vndEntity => {
        stations.push(Station.createFromVndEntity(vndEntity));
      });

      return stations;
    });
  }
}
